﻿using System.Configuration;

namespace ColabHistory.Common
{
    public class Config
    {
        public static string GetKey(string keyName)
        {
            return ConfigurationManager.AppSettings[keyName];
        }

        public static string ElasticSearchDbConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["WappaElasticSearchDb"].ToString(); }
        }
    }
}
