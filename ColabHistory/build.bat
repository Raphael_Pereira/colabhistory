for %%a in (.) do set currentfolder=%%~na

for /R %cd% %%f in (*.csproj) do set filename=%%f 

cd C:\Packages\%currentfolder%
del /q *.nupkg

cd %cd%
"C:\Program Files (x86)\MSBuild\12.0\Bin\MSBuild.exe" %filename% /t:Rebuild /p:Configuration=Release /p:RunOctoPack=true /p:OctoPackPublishPackageToFileShare=C:\Packages\%currentfolder% /p:OctoPackEnforceAddingFiles=true
pause