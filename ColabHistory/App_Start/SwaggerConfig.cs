﻿using Swashbuckle.Application;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ColabHistory
{
    public class SwaggerConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config
                .EnableSwagger(c =>
                {
                    c.RootUrl(requestMessage => requestMessage.RequestUri.GetLeftPart(UriPartial.Authority) + requestMessage.GetRequestContext().VirtualPathRoot.TrimEnd('/'));
                    c.SingleApiVersion("v1", "ColabHistory Api");
                })
                .EnableSwaggerUi();
        }
    }
}