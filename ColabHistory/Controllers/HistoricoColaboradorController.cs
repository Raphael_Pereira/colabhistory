﻿using ColabHistory.Contract.Request;
using ColabHistory.Contract.Service;
using System.Linq;
using System.Web.Http;
using Wappa.LogApiProvider;

namespace ColabHistory.Controllers
{
    [RoutePrefix("api/historico-colaborador")]
    public class HistoricoColaboradorController : ApiController
    {
        private readonly Logger _log;
        private readonly IHistoricoColaboradorService _svcHistoricoColaborador;

        public HistoricoColaboradorController(Logger log, IHistoricoColaboradorService svcHistoricoColaborador)
        {
            _log = log;
            _svcHistoricoColaborador = svcHistoricoColaborador;
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Salvar([FromUri]HistoricoColaboradorRequest request)
        {
            var resp = _svcHistoricoColaborador.Salvar(request.IdUsuario, request.PlaceId, request.Categoria);

            if (resp != null && resp.IdUsuario > 0)
                return Ok(resp);
            else
                return BadRequest();
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Consultar([FromUri]HistoricoColaboradorRequest request)
        {
            var resp = _svcHistoricoColaborador.Consultar(request.IdUsuario,
                                                          request.PlaceId,
                                                          request.Estado,
                                                          request.Pais);

            if (resp != null && resp.IdUsuario > 0)
                return Ok(resp);
            else
                return BadRequest();
        }
    }
}