﻿using Autofac;
using Autofac.Integration.WebApi;
using ColabHistory.Application;
using ColabHistory.Common;
using ColabHistory.DI.Modules;
using ColabHistory.Infrastructure;
using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Owin;
using System;
using System.IdentityModel.Tokens;
using System.Reflection;
using System.Web.Http;
using Wappa.LogApiProvider;
using Wappa.OwinMiddleware.LogApi;
using Wappa.OwinMiddleware.LogApi.Builders;
using WappaCommon.HttpService.Service;

[assembly: OwinStartup(typeof(ColabHistory.Startup))]

namespace ColabHistory
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            RegisterDependencies(app, config);

            ConfigIdentityServer(app);
            
            WebApiConfig.Register(config);
            SwaggerConfig.Register(config);

            app.UseWebApi(config);
        }

        private static void ConfigIdentityServer(IAppBuilder app)
        {
            JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();

            var baseUri = new Uri(Config.GetKey("AuthenticationApiUrl"));
            var uri = new Uri(baseUri, Config.GetKey("AuthenticationApiBase"));

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = uri.ToString(),
                RequiredScopes = new[] { "ColabHistory" }
            });
        }

        public static void RegisterDependencies(IAppBuilder app, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule(new LogDependencyModule());

            builder.RegisterAssemblyTypes(typeof(HistoricoColaboradorRepository).Assembly)
                   .Where(t => t.Name.EndsWith("Repository"))
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(HistoricoColaboradorService).Assembly)
                   .Where(t => t.Name.EndsWith("Service"))
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(HttpService).Assembly)
                   .Where(t => t.Name.EndsWith("Service") && !t.IsAbstract)
                   .AsImplementedInterfaces()
                   .AsSelf()
                   .InstancePerLifetimeScope();

            builder.RegisterWebApiFilterProvider(config);

            var container = builder.Build();

            var logger = container.Resolve<Logger>();

            var loggerSettings = new LogMiddlewareSettingsBuilder()
                .WithPathBlacklist("swagger")
                .Build();

            app.Use<LogMiddleware>(logger, loggerSettings);

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
