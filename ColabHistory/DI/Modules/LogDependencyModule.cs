﻿using Autofac;
using ColabHistory.Common;
using Wappa.OwinMiddleware.LogApi;

namespace ColabHistory.DI.Modules
{
    public class LogDependencyModule : Module
    {
        private const string ApplicationName = "ColabHistory";

        protected override void Load(ContainerBuilder builder)
        {
            var apiEndpoint = Config.GetKey("LogApiUrl");
            builder.RegisterType<LogMiddleware>();
            builder.Register(e => new Wappa.LogApiProvider.Logger(apiEndpoint, ApplicationName));
        }
    }
}