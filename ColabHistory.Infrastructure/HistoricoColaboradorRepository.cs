﻿using ColabHistory.Contract.Repository;
using ColabHistory.Domain.Elasticsearch;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ColabHistory.Infrastructure
{
    public class HistoricoColaboradorRepository : Repository, IHistoricoColaboradorRepository
    {
        private readonly string _index;
        private readonly IElasticClient _client;

        public HistoricoColaboradorRepository()
        {
            _client = CreateElasticSearchConnection();
            _index = "historico_colaborador";

            if (!FindElasticSearchIndex(_client, _index))
                CreateIndex<HistoricoColaborador>(_client, _index);
        }

        public bool Salvar(HistoricoColaborador info)
        {
            var ret = _client.Index(info, c => c.Index(_index)
                                                .Type("character")
                                                .Id(info.Id)
                                                .Refresh(Elasticsearch.Net.Refresh.True));

            return ret.IsValid;
        }

        public HistoricoColaborador ObterFavoritos(long idUsuario)
        {
            var fltMust = new List<Func<QueryContainerDescriptor<HistoricoColaborador>, QueryContainer>>
            {
                c => c.Term(f => f.Field(t => t.IdUsuario).Value(idUsuario)),
                c => c.Term(f => f.Field(t => t.Favorite).Value(true))
            };

            var ret = _client.Search<HistoricoColaborador>(c => c.Index(_index)
                                                                 .Type("character")
                                                                 .Query(f => f.Bool(t => t.Must(fltMust))));

            var rawQuery = Encoding.UTF8.GetString(ret.ApiCall.RequestBodyInBytes);

            if (ret.Hits != null && ret.Hits.Any() && ret.Hits.FirstOrDefault().Source != null)
                return ret.Hits.FirstOrDefault().Source;
            else
                return new HistoricoColaborador();
        }

        public List<HistoricoColaborador> ObterHistorico(long idUsuario, string estado, string pais)
        {
            estado = string.Format("*{0}*", estado.ToLower());
            pais = string.Format("*{0}*", pais.ToLower());

            var fltMust = new List<Func<QueryContainerDescriptor<HistoricoColaborador>, QueryContainer>>
            {
                c => c.Term(f => f.Field(t => t.IdUsuario).Value(idUsuario)),
                c => c.Term(f => f.Field(t => t.Favorite).Value(false)),
                c => c.Wildcard(f => f.Field(t => t.State).Value(estado)),
                c => c.Wildcard(f => f.Field(t => t.Country).Value(pais))
            };

            var ret = _client.Search<HistoricoColaborador>(c => c.Index(_index)
                                                                 .Type("character")
                                                                 .Query(f => f.Bool(t => t.Must(fltMust))));

            var rawQuery = Encoding.UTF8.GetString(ret.ApiCall.RequestBodyInBytes);

            if (ret != null && ret.Hits != null)
            {
                var resp = new List<HistoricoColaborador>();

                ret.Hits.ToList().ForEach(c =>
                {
                    if (c.Source != null)
                        resp.Add(c.Source);
                });

                return resp;
            }
            else
                return new List<HistoricoColaborador>();
        }

        public bool Apagar(HistoricoColaborador info)
        {
            var ret = _client.Delete<HistoricoColaborador>(info.Id);

            return ret.Found;
        }
    }
}
