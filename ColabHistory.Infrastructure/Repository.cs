﻿using ColabHistory.Common;
using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Net;

namespace ColabHistory.Infrastructure
{
    public class Repository
    {
        protected virtual IElasticClient CreateElasticSearchConnection()
        {
            var node = new Uri(Config.ElasticSearchDbConnectionString);
            var settings = new ConnectionSettings(new SingleNodeConnectionPool(node), new HttpConnection()).DisableDirectStreaming();
            var client = new ElasticClient(settings);

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, errors) => true;

            return client;
        }

        public bool FindElasticSearchIndex(IElasticClient client, string indice)
        {
            var index = Indices.Index(new IndexName { Name = indice });
            var indiceBusca = client.IndexExists(new IndexExistsRequest(index));

            return indiceBusca.Exists;
        }

        public bool CreateIndex<T>(IElasticClient client, string indexName) where T : class
        {
            var indexCreated = true;

            var index = new CreateIndexDescriptor(new IndexName() { Name = indexName }).Mappings(ms => ms.Map<T>(m => m.AutoMap()));
            var configSetting = new IndexSettings { { "NumberOfShards", 10 }, { "NumberOfReplicas", 2 } };
            var listaFilter = new List<string> { "standard", "lowercase", "stop" };

            var an = new CustomAnalyzer();
            an.CharFilter = new List<string>();
            an.Tokenizer = "edgeNGram";
            an.Filter = listaFilter;

            var indexConfig = new IndexState { Settings = configSetting };

            if (index == null)
                return true;

            try
            {
                client.CreateIndex(indexName, i => index.InitializeUsing(indexConfig).Mappings(ms => ms.Map<T>(m => m.AutoMap())));
            }
            catch (Exception ex)
            {
                indexCreated = false;
            }

            return indexCreated;
        }
    }
}
