﻿namespace ColabHistory.Contract.Request
{
    public class HistoricoColaboradorRequest
    {
        public long IdUsuario { get; set; }
        public string Categoria { get; set; }
        public string PlaceId { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
    }
}
