﻿using ColabHistory.Domain.Proxy;
using System.Collections.Generic;

namespace ColabHistory.Contract.Response
{
    public class HistoricoColaboradorResponse
    {
        public long IdUsuario { get; set; }
        public List<Address> History { get; set; }
        public List<Address> Favorites { get; set; }
    }
}
