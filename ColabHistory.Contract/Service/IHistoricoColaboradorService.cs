﻿using ColabHistory.Contract.Response;

namespace ColabHistory.Contract.Service
{
    public interface IHistoricoColaboradorService
    {
        HistoricoColaboradorResponse Consultar(long idUsuario, string placeId, string estado, string pais);
        HistoricoColaboradorResponse Salvar(long idUsuario, string placeId, string categoria);
    }
}
