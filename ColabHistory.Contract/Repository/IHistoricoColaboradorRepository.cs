﻿using ColabHistory.Domain.Elasticsearch;
using System.Collections.Generic;

namespace ColabHistory.Contract.Repository
{
    public interface IHistoricoColaboradorRepository
    {
        bool Salvar(HistoricoColaborador info);
        HistoricoColaborador ObterFavoritos(long idUsuario);
        List<HistoricoColaborador> ObterHistorico(long idUsuario, string estado, string pais);
        bool Apagar(HistoricoColaborador info);
    }
}
