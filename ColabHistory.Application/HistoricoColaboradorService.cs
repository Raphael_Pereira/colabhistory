﻿using ColabHistory.Common;
using ColabHistory.Contract.Repository;
using ColabHistory.Contract.Response;
using ColabHistory.Contract.Service;
using ColabHistory.Domain.Elasticsearch;
using ColabHistory.Domain.Proxy;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wappa.LogApiProvider;
using WappaCommon.HttpService.Interface;

namespace ColabHistory.Application
{
    public class HistoricoColaboradorService : IHistoricoColaboradorService
    {
        private readonly Logger _log;
        private readonly IHttpService _svcHttp;
        private readonly IHistoricoColaboradorRepository _repHistoricoColaborador;

        public HistoricoColaboradorService(Logger log, IHistoricoColaboradorRepository repHistoricoColaborador, IHttpService svcHttp)
        {
            _log = log;
            _svcHttp = svcHttp;
            _repHistoricoColaborador = repHistoricoColaborador;
        }

        public HistoricoColaboradorResponse Salvar(long idUsuario, string placeId, string categoria)
        {
            #region Buscar dados do place id
            var state = string.Empty;
            var country = string.Empty;

            var address = BuscarDadosPlaceId(placeId, out state, out country);
            #endregion

            var isValid = false;

            #region Salvar informações
            #region Favoritos
            var refFav = _repHistoricoColaborador.ObterFavoritos(idUsuario);

            var novoFav = true;

            if (refFav != null && refFav.Addresses != null)
            {
                Address removeFav = null;

                //Verifica se a localização está na lista de favoritos
                refFav.Addresses.ForEach(c =>
                {
                    if (c.result.place_id.Equals(placeId))
                    {
                        if (string.IsNullOrEmpty(categoria))
                            removeFav = c;
                        else
                            c.Category = categoria;

                        novoFav = false;
                    }
                });

                if (removeFav != null)
                    refFav.Addresses.Remove(removeFav);
            }

            if (novoFav && !string.IsNullOrEmpty(categoria))
            {
                address.Category = categoria;
                address.Date = DateTime.Now;

                //Verifica se há uma lista de favoritos, caso não, cria uma
                if (refFav != null)
                {
                    if (refFav.Addresses != null)
                        refFav.Addresses.Add(address);
                    else
                    {
                        refFav.Id = (refFav.Id ?? Guid.NewGuid().ToString());
                        refFav.Favorite = true;
                        refFav.IdUsuario = (refFav.IdUsuario > 0 ? refFav.IdUsuario : idUsuario);
                        refFav.Country = string.Empty;
                        refFav.State = string.Empty;
                        refFav.Addresses = new List<Address> {
                            new Address {
                                Category = address.Category,
                                Date = address.Date,
                                result = address.result,
                                status = address.status
                            }
                        };
                    }
                }
                else
                {
                    refFav = new HistoricoColaborador
                    {
                        Id = Guid.NewGuid().ToString(),
                        Favorite = true,
                        IdUsuario = idUsuario,
                        Country = string.Empty,
                        State = string.Empty,
                        Addresses = new List<Address>()
                    };

                    refFav.Addresses.Add(address);
                }
            }

            if (refFav.IdUsuario > 0)
                isValid = _repHistoricoColaborador.Salvar(refFav);
            #endregion

            #region Histórico
            var retHist = _repHistoricoColaborador.ObterHistorico(idUsuario, state, country);

            var hist = new HistoricoColaborador();
            var novoHist = true;

            if (retHist != null && retHist.Any() && retHist.FirstOrDefault().Addresses != null)
            {
                //Verifica se a localização está na lista de histórico
                retHist.ForEach(c => c.Addresses.ForEach(t =>
                {
                    if (t.result.place_id.Equals(placeId))
                    {
                        t.Date = DateTime.Now;

                        hist = c;

                        novoHist = false;
                    }
                }));
            }

            if (novoHist)
            {
                address.Category = string.Empty;
                address.Date = DateTime.Now;

                //Verifica se a lista de histórico tem mais de 4 registros, caso sim, deleta o registro mais antigo
                if (retHist != null && retHist.Any() && retHist.FirstOrDefault().Addresses != null)
                {
                    hist = retHist.FirstOrDefault();

                    if (hist.Addresses.Count() > 4)
                        hist.Addresses.Remove(hist.Addresses.OrderByDescending(c => c.Date).Last());

                    hist.Addresses.Add(address);
                }
                else
                {
                    hist.Id = Guid.NewGuid().ToString();
                    hist.Favorite = false;
                    hist.IdUsuario = idUsuario;
                    hist.Country = country;
                    hist.State = state;
                    hist.Addresses = new List<Address>();

                    hist.Addresses.Add(address);
                }
            }

            isValid = _repHistoricoColaborador.Salvar(hist);
            #endregion
            #endregion

            if (isValid)
                return MontarRetorno(idUsuario);
            else
                return new HistoricoColaboradorResponse();
        }

        public HistoricoColaboradorResponse Consultar(long idUsuario, string placeId, string estado, string pais)
        {
            if (!string.IsNullOrEmpty(placeId))
                BuscarDadosPlaceId(placeId, out estado, out pais);

            estado = TratarPalavraParaElasticSearch(estado);
            pais = TratarPalavraParaElasticSearch(pais);

            return MontarRetorno(idUsuario, estado, pais);
        }

        private Address BuscarDadosPlaceId(string placeId, out string estado, out string pais)
        {
            #region Obter dados do place id
            var urlProxy = Config.GetKey("ProxyApi");
            var uriProxy = Config.GetKey("ProxyApiUriPlaceId");

            var param = new Dictionary<string, object>();
            param.Add("placeid", placeId);

            var address = _svcHttp.Get<Address>(urlProxy, uriProxy, param);
            #endregion

            #region Filtrar informações
            var state = string.Empty;
            var country = string.Empty;

            if (address != null && address.result != null)
            {
                address.result.address_components.ForEach(c =>
                {
                    if (c.types.Contains("administrative_area_level_1"))
                        state = TratarPalavraParaElasticSearch(c.long_name);

                    if (c.types.Contains("country"))
                        country = TratarPalavraParaElasticSearch(c.long_name);
                });
            }
            #endregion

            estado = state;
            pais = country;

            return address;
        }

        private HistoricoColaboradorResponse MontarRetorno(long idUsuario, string estado = null, string pais = null)
        {
            var lstFavorites = new List<Address>();
            var lstHistory = new List<Address>();

            var retFav = _repHistoricoColaborador.ObterFavoritos(idUsuario);
            var retHist = _repHistoricoColaborador.ObterHistorico(idUsuario, (estado ?? string.Empty), (pais ?? string.Empty));

            if (retFav != null && retFav.Addresses != null)
                lstFavorites = retFav.Addresses;

            if (retHist != null && retHist.Any())
            {
                retHist.ForEach(c => c.Addresses.ForEach(f => lstHistory.Add(f)));

                lstHistory = lstHistory.OrderByDescending(c => c.Date).Take(5).ToList();
            }

            var resp = new HistoricoColaboradorResponse
            {
                IdUsuario = idUsuario,
                Favorites = lstFavorites,
                History = lstHistory
            };

            return resp;
        }

        private string TratarPalavraParaElasticSearch(string texto)
        {
            if (string.IsNullOrEmpty(texto))
                return texto;

            var result = new StringBuilder();
            var arrayText = texto.Normalize(NormalizationForm.FormD).ToCharArray();

            foreach (var letter in arrayText)
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    result.Append(letter);

            return result.ToString().Trim().Replace(" ", "_").ToLower();
        }
    }
}
