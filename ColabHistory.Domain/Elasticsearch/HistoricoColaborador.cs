﻿using ColabHistory.Domain.Proxy;
using System.Collections.Generic;

namespace ColabHistory.Domain.Elasticsearch
{
    public class HistoricoColaborador
    {
        public string Id { get; set; }
        public long IdUsuario { get; set; }
        public bool Favorite { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public List<Address> Addresses { get; set; }
    }
}
